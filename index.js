GM_addStyle(`
  /* width */
  #helper-button ::-webkit-scrollbar {
    width: 6px !important;
  }
  /* Track */
  #helper-button ::-webkit-scrollbar-track {
    border-radius: 10px !important;
  }
  /* Handle */
  #helper-button ::-webkit-scrollbar-thumb {
    background: #f3f3f3 !important;
    border-radius: 10px !important;
  }
  /* Handle on hover */
  #helper-button ::-webkit-scrollbar-thumb:hover {
    background: lightgray !important;
  }
  #helper-button {
    display: flex !important;
    flex-direction: column !important;
    right: 10px !important;
    bottom: 10px !important;
    position: fixed !important;
    z-index: 999999999 !important;
    border-radius: 20px !important;
    box-shadow: rgba(0, 0, 0, 0.35) 0px 5px 15px !important;
    background-color: white !important;
    font-family: cursive !important;
    text-align: left !important;
  }
  #helper-button.helper-button-hover {
    cursor: pointer !important;
  }
  #helper-button.helper-button {
    align-items: center !important;
    justify-content: center !important;
    height: 30px !important;
    width: 80px !important;
    transition: 0.35s ease !important;
  }
  #helper-button.helper-button-open {
    height: 400px !important;
    width: 250px !important;
    transition: 0.35s ease !important;
    padding: 15px 15px !important;
    border-radius: 10px !important;;
    justify-content: space-between !important;
  }
  #helper-button .helper-title {
    display: flex !important;
    justify-content: space-between !important;
    user-select: none !important;
    font-weight: 700 !important;
  }
  #helper-button .helper-subtitle-gap,
  #helper-button .helper-subtitle {
    font-weight: 700 !important;
  }
  #helper-button .helper-subtitle {
    font-size: medium !important;
  }
  #helper-button .helper-title-open {
    font-size: 20px !important;
    margin: auto 8px !important;
  }
  #helper-button .helper-btn-bar {
    display: flex !important;
  }
  #helper-button .helper-btn-setting,
  #helper-button .helper-btn-create,
  #helper-button .helper-btn-close,
  #helper-button .helper-btn-back,
  #helper-button .helper-btn-apply {
    height: 36px !important;
    width: 36px !important;
    padding: 8px 8px !important;
    margin: auto 0 !important;
    color: gray !important;
    border-radius: 30px !important;
    transition: 0.2s ease !important;
  }
  #helper-button .helper-btn-apply {
    height: 28px !important;
    width: 28px !important;
    padding: 4px 4px !important;
  }
  #helper-button .helper-btn-setting:hover,
  #helper-button .helper-btn-create:hover,
  #helper-button .helper-btn-close:hover,
  #helper-button .helper-btn-back:hover,
  #helper-button .helper-btn-apply:hover {
    color: black !important;
    background-color: #f3f3f3 !important;
    cursor: pointer !important;
    transition: 0.2s ease !important;
  }
  #helper-button .helper-btn-apply:hover {
    background-color: lightgray !important;
  }
  #helper-button .helper-main-list,
  #helper-button .helper-edit-item,
  #helper-button .helper-create-item,
  #helper-button .helper-setting  {
    height: 330px !important;
    width: 220px !important;
    overflow-y: auto !important;
    transition: 0.3s ease !important;
  }
  #helper-button .group {
    margin-top: 8px !important;
  }
  #helper-button .group-title {
    display: flex !important;
    align-items: center !important;
    justify-content: space-between !important;
    padding: 4px 4px !important;
    margin-bottom: 4px !important;
    margin-right: 4px !important;
    font-weight: 700 !important;
    font-size: medium !important;
    background-color: #f3f3f3 !important;
    border-left: 8px solid lightgray !important;
    border-right: 8px solid #f3f3f3 !important;
    border-radius: 5px !important;
  }
  #helper-button .action {
    display: flex !important;
    justify-content: space-between !important;
    align-items: center !important;
    padding: 0 8px !important;
    margin: 0 4px !important;
    user-select: none !important;
  }
  #helper-button .action-name {
    width: 150px !important;
    padding: 8px !important;
    color: gray !important;
    font-size: medium !important;
    border-radius: 10px !important;
    transition: 0.2s ease !important;
  }
  #helper-button .action-name:hover {
    color: black !important;
    background-color: #f3f3f3 !important;
    transition: 0.2s ease !important;
    cursor: pointer !important;
    font-weight: 700 !important;
    transform: scale(1.1) !important;
  }
  #helper-button .action-edit {
    display: flex !important;
    height: 36px !important;
    width: 36px !important;
    align-items: center !important;
    border-radius: 30px !important;
    padding: 8px !important;
    transition: 0.2s ease !important;
  }
  #helper-button .action-edit:hover {
    background-color: #f3f3f3 !important;
    transition: 0.2s ease !important;
    cursor: pointer !important;
    font-weight: 700 !important;
  }
  #helper-button .action-edit > svg > path {
    stroke: gray !important;
  }
  #helper-button .helper-create-item,
  #helper-button .helper-edit-item {
    display: flex !important;
    flex-direction: column !important;
    justify-content: space-between !important;
    padding: 8px !important;
  }
  #helper-button .input-group,
  #helper-button .edit-input-group {
    display: flex !important;
    flex-direction: column !important;
  }
  #helper-button .input-item {
    display: flex !important;
    flex-direction: column !important;
    margin: 4px 0 !important;
  }
  #helper-button .input-item > label {
    margin-bottom: 4px !important;
  }
  #helper-button .input-item > input {
    max-width: 100% !important;
    border-top: none !important;
    border-left: none !important;
    border-right: none !important;
    border-bottom: 1px solid lightgray !important;
  }
  #helper-button .input-item input[type="checkbox"] {
    display: inline-block !important;
    margin-top: unset !important;
  }
  #helper-button .input-item > input:focus {
    outline: none !important;
  }
  #helper-button .input-item.wrong-input > label,
  #helper-button .input-item.wrong-input > span {
    color: red !important;
  }
  #helper-button .input-item.wrong-input > input {
    border-bottom: 1px solid red !important;
  }
  #helper-button .input-item.wrong-input-shaking > label,
  #helper-button .input-item.wrong-input-shaking > input,
  #helper-button .input-item.wrong-input-shaking > span {
    animation: horizontal-shaking 0.2s !important;
  }

  @keyframes horizontal-shaking {
    0% {
      transform: translateX(0) !important;
    }
    25% {
      transform: translateX(5px) !important;
    }
    50% {
      transform: translateX(-5px) !important;
    }
    75% {
      transform: translateX(5px) !important;
    }
    100% {
      transform: translateX(0) !important;
    }
  }
  #helper-button .action-button-area {
    display: flex !important;
    flex-direction: column !important;
  }
  #helper-button .action-button {
    display: flex !important;
    justify-content: space-evenly !important;
  }
  #helper-button .action-button > .delete-button,
  #helper-button .action-button > .save-button,
  #helper-button .action-button > .clear-button {
    flex: 1 !important;
    user-select: none !important;
    padding: 8px 4px !important;
    margin: 0 4px !important;
    text-align: center !important;
    border-radius: 10px !important;
    background-color: #f3f3f3 !important;
    transition: 0.2s ease infinite !important;
  }
  #helper-button .action-button > .delete-button:hover,
  #helper-button .action-button > .save-button:hover,
  #helper-button .action-button > .clear-button:hover {
    cursor: pointer !important;
    transition: 0.2s ease !important;
    transform: scale(1.1) !important;
  }
  #helper-button .helper-setting {
    display: flex !important;
    flex-direction: column !important;
  }
  #helper-button .helper-setting > .upload-setting,
  #helper-button .helper-setting > .download-setting {
    flex: 1 !important;
    display: flex !important;
    justify-content: center !important;
    align-items: center !important;
    border: 1px dashed lightgray !important;
    border-radius: 10px !important;
    margin: 10px !important;
    transition: 0.2s ease !important;
  }
  #helper-button .helper-setting > .upload-setting:hover,
  #helper-button .helper-setting > .download-setting:hover {
    background-color: #f3f3f3 !important;
    cursor: pointer !important;
    transition: 0.2s ease !important;
  }
  #input-upload-file {
    display: none !important;
  }

`);

(function () {
  "use strict";
  const vContainer = document.createElement("div");
  const wcBody = document.querySelector("body");
  wcBody.appendChild(vContainer);

  const app = Vue.createApp({
    data() {
      return {
        pageStack: ["close"],
        errors: {
          group: { error: false, shaking: false },
          name: { error: false, shaking: false },
          selector: { error: false, shaking: false },
          value: { error: false, shaking: false },
        },
        action: {
          group: "  ",
          name: "",
          selector: "",
          value: { click: false, text: "" },
        },
        groupList: [],
        actionList: [],
      };
    },
    created() {
      const initData = async () => {
        const savedActionList = await this.getLocalStorage("actionList");
        // console.log(typeof savedActionList);
        // console.log(savedActionList);
        if (typeof savedActionList === "object") {
          this.actionList = savedActionList;
        } else {
          await this.setLocalStorage("actionList", []);
        }
        this.actionListConverter();
      };
      initData();
    },
    watch: {
      actionList: {
        handler(oldActionList, newActionList) {
          // console.log(oldActionList);
          // console.log(newActionList);
          // console.log("action List updated");
          this.actionListConverter();
        },
        deep: true,
      },
    },
    computed: {
      helperClassObj() {
        return {
          "helper-button": this.currentPage === "close",
          "helper-button-hover": this.currentPage === "close",
          "helper-button-open": this.currentPage !== "close",
        };
      },
      helperTitleOpenClassObj() {
        return {
          "helper-title-open": this.currentPage !== "close",
        };
      },
      helperCreateEditClassObj() {
        return {
          "helper-create-item": this.currentPage === "create",
          "helper-edit-item": this.currentPage === "edit",
        };
      },
      groupErrorClassObj() {
        return {
          "wrong-input": this.errors.group.error,
          "wrong-input-shaking": this.errors.group.shaking,
        };
      },
      nameErrorClassObj() {
        return {
          "wrong-input": this.errors.name.error,
          "wrong-input-shaking": this.errors.name.shaking,
        };
      },
      selectorErrorClassObj() {
        return {
          "wrong-input": this.errors.selector.error,
          "wrong-input-shaking": this.errors.selector.shaking,
        };
      },
      valueErrorClassObj() {
        return {
          "wrong-input": this.errors.value.error,
          "wrong-input-shaking": this.errors.value.shaking,
        };
      },
      currentPage() {
        // console.log("Current Page: ", this.pageStack.at(-1));
        return this.pageStack.at(-1);
      },
      isNoOpen() {
        return ["create", "edit", "setting"].includes(this.currentPage);
      },
    },
    methods: {
      setCurrentPage(value) {
        if (!this.pageStack.includes(value)) {
          this.pageStack.push(value);
          if (this.currentPage === "create") {
            this.resetAction();
          }
          console.log("Page Stack: ", this.pageStack);
        }
      },
      backToPrevPage() {
        this.pageStack.pop();
        this.resetErrors();
      },
      resetErrors() {
        for (let idx in this.errors) {
          this.errors[idx].error = false;
          // this.errors[idx].shaking = false;
        }
        // console.log("reset errors", this.errors);
      },
      resetErrorAnimation() {
        setTimeout(() => {
          for (let idx in this.errors) {
            this.errors[idx].shaking = false;
          }
          // console.log("reset error animation", this.errors);
        }, 200);
      },
      retrieveActionForEdit(groupName, actionName) {
        // console.log("retrieveActionForEdit", groupName, actionName);
        const { action } = this.findAction(groupName, actionName);
        // console.log("retrieveActionForEdit action", action);
        this.setCurrentPage("edit");
        this.action.group = action.group;
        this.action.name = action.name;
        this.action.selector = action.selector;
        this.action.value = action.value;
      },
      findAction(groupName, actionName) {
        const action = this.actionList.find(
          (item) => item.group === groupName && item.name === actionName
        );
        const index = this.actionList.indexOf(action);
        return { index, action };
      },
      findActionByGroup(groupName) {
        const group = this.actionList.filter(
          (item) => item.group === groupName
        );
        return group;
      },
      deleteAction() {
        // console.log(this.action);
        const actionListFilter = this.actionList.filter((item) => {
          // console.log(
          //   "item: ",
          //   item,
          //   item.group !== this.action.group || item.name !== this.action.name
          // );
          return (
            item.group !== this.action.group || item.name !== this.action.name
          );
        });
        // console.log("actionListFilter： ", actionListFilter);
        this.actionList = actionListFilter;
        this.resetAction();
        this.setLocalStorage("actionList", this.actionList);
        this.backToPrevPage();
      },
      saveAction() {
        // console.log(this.currentPage);
        if (this.currentPage === "create") {
          // reset errors
          this.resetErrors();
          // input validation
          if (this.inputValidation()) {
            this.resetErrorAnimation();
            return;
          }
          // create action
          this.createAction();
          // reset action
          this.resetAction();
          this.setLocalStorage("actionList", this.actionList);
          this.backToPrevPage();
        } else if (this.currentPage === "edit") {
          // reset errors
          this.resetErrors();
          // input validation
          if (this.inputValidation()) {
            this.resetErrorAnimation();
            return;
          }
          const { index } = this.findAction(
            this.action.group,
            this.action.name
          );
          this.actionList[index].selector = this.action.selector;
          this.actionList[index].value = this.action.value;
          this.setLocalStorage("actionList", this.actionList);
        }
      },
      clearAction() {
        // reset errors
        this.resetErrors();
        // reset action
        this.resetAction();
      },
      resetAction() {
        for (let idx in this.action) {
          if (
            !(
              this.pageStack.at(-1) === "edit" &&
              ["group", "name"].includes(idx)
            )
          )
            this.action[idx] =
              idx === "value" ? { click: false, text: "" } : "";
        }
        // console.log("reset action");
      },
      inputValidation() {
        const errorlist = [];
        for (let idx in this.action) {
          console.log(this.action);
          console.log(this.action[idx]);
          let result = false;
          if (idx === "value") {
            // console.log(this.action[idx]);
            result =
              !this.action[idx].click &&
              (this.action[idx].text === null || this.action[idx].text === "");
          } else {
            result = this.action[idx] === null || this.action[idx] === "";
          }
          this.errors[idx].error = result;
          this.errors[idx].shaking = result;
          console.log(idx, result);
          errorlist.push(result);
        }
        // console.log("this.errors", this.errors);
        // console.log("errorlist", errorlist);
        return errorlist.includes(true);
      },
      createAction() {
        const action = {
          group: this.action.group,
          name: this.action.name,
          selector: this.action.selector,
          value: this.action.value,
        };
        // console.log(action);
        this.actionList.push(action);
        // console.log(this.actionList);
      },
      actionListConverter() {
        // sort action list
        const actionListClone = JSON.parse(JSON.stringify(this.actionList));
        actionListClone.sort(
          (a, b) =>
            a.group.localeCompare(b.group) || a.name.localeCompare(b.name)
        );
        // get group from actionList
        const groups = Array.from(
          new Set(actionListClone.map((item) => item.group))
        );
        // console.log("groups", groups);
        const groupList = [];
        groups.forEach((group) => {
          const actions = actionListClone.reduce((result, action) => {
            if (action.group === group) {
              result.push({
                name: action.name,
                selector: action.selector,
                value: action.value,
              });
            }
            return result;
          }, []);
          // console.log("actions", actions);
          groupList.push({ groupName: group, actionList: actions });
        });
        // console.log("groupList", JSON.stringify(groupList));
        // return groupList;
        this.groupList = groupList;
      },
      applyValue(groupName, actionName) {
        const { action } = this.findAction(groupName, actionName);
        this.setValueBySelector(action.selector, action.value);
      },
      appleGroupValue(groupName) {
        const group = this.findActionByGroup(groupName);
        // console.log(group);
        group.forEach((action) => {
          this.setValueBySelector(action.selector, action.value);
        });
      },
      setValueBySelector(selector, value) {
        const elms = document.querySelectorAll(selector);
        elms.forEach((elm) => {
          value.click ? elm.click() : (elm.value = value.text);
        });
      },
      async getLocalStorage(key) {
        const data = await localStorage.getItem(key);
        if (data) {
          return JSON.parse(data);
        }
        return "";
      },
      async setLocalStorage(key, value) {
        await localStorage.setItem(key, JSON.stringify(value));
      },
      setTextValueById(elm, value) {
        if (elm) elm.value = value;
      },
      uploadClick() {
        document.querySelector("#input-upload-file").click();
      },
      uploadSetting(event) {
        const files = event.target.files;
        if (files.length === 0) {
          alert("No File Selected");
          return;
        }
        const file = files[0];
        console.log(file);
        if (file.type !== "application/json") {
          alert("Accept JSON file Only");
          return;
        }
        const reader = new FileReader();
        reader.readAsText(file, "UTF-8");
        reader.onload = (e) => {
          try {
            // console.log(e.target.result);
            const fileJson = JSON.parse(e.target.result);
            this.actionList = fileJson;
            this.setLocalStorage("actionList", this.actionList);
            alert("Upload Setting Success");
          } catch (e) {
            alert(e);
          }
        };
        reader.onerror = (e) => {
          // console.log("Error Reading File:", e);
          alert("Error Reading File", e);
        };
      },
      downloadSetting() {
        const actionBlob = new Blob([JSON.stringify(this.actionList)], {
          type: "application/json",
        });
        const blobUrl = URL.createObjectURL(actionBlob);
        const link = document.createElement("a");
        link.href = blobUrl;
        link.download = `${window.location.hostname}.fill.js.json`;

        const clickHandler = () => {
          setTimeout(() => {
            URL.revokeObjectURL(blobUrl);
            link.removeEventListener("click", clickHandler);
          }, 150);
        };
        link.addEventListener("click", clickHandler, false);
        link.click();
      },
    },
    template: `
      <div id="helper-button" :class="helperClassObj" @click.stop="setCurrentPage('open')">
        <div class="helper-title">
          <div :class="helperTitleOpenClassObj">
            <span>Fill.js</span>
            <span class="helper-subtitle-gap" v-if="isNoOpen">/</span>
            <span class="helper-subtitle" v-if="currentPage==='setting'">Setting</span>
            <span class="helper-subtitle" v-if="currentPage==='create'">Create</span>
            <span class="helper-subtitle" v-if="currentPage==='edit'">Edit</span>
          </div>
          <div class="helper-btn-bar" v-if="currentPage!=='close'">
          <span v-if="currentPage==='open'" class="helper-btn-setting" @click.stop="setCurrentPage('setting')">
          <svg width="20px" height="20px" viewBox="0 0 1024 1024" xmlns="http://www.w3.org/2000/svg">
              <path fill="#000000"
                  d="M600.704 64a32 32 0 0 1 30.464 22.208l35.2 109.376c14.784 7.232 28.928 15.36 42.432 24.512l112.384-24.192a32 32 0 0 1 34.432 15.36L944.32 364.8a32 32 0 0 1-4.032 37.504l-77.12 85.12a357.12 357.12 0 0 1 0 49.024l77.12 85.248a32 32 0 0 1 4.032 37.504l-88.704 153.6a32 32 0 0 1-34.432 15.296L708.8 803.904c-13.44 9.088-27.648 17.28-42.368 24.512l-35.264 109.376A32 32 0 0 1 600.704 960H423.296a32 32 0 0 1-30.464-22.208L357.696 828.48a351.616 351.616 0 0 1-42.56-24.64l-112.32 24.256a32 32 0 0 1-34.432-15.36L79.68 659.2a32 32 0 0 1 4.032-37.504l77.12-85.248a357.12 357.12 0 0 1 0-48.896l-77.12-85.248A32 32 0 0 1 79.68 364.8l88.704-153.6a32 32 0 0 1 34.432-15.296l112.32 24.256c13.568-9.152 27.776-17.408 42.56-24.64l35.2-109.312A32 32 0 0 1 423.232 64H600.64zm-23.424 64H446.72l-36.352 113.088-24.512 11.968a294.113 294.113 0 0 0-34.816 20.096l-22.656 15.36-116.224-25.088-65.28 113.152 79.68 88.192-1.92 27.136a293.12 293.12 0 0 0 0 40.192l1.92 27.136-79.808 88.192 65.344 113.152 116.224-25.024 22.656 15.296a294.113 294.113 0 0 0 34.816 20.096l24.512 11.968L446.72 896h130.688l36.48-113.152 24.448-11.904a288.282 288.282 0 0 0 34.752-20.096l22.592-15.296 116.288 25.024 65.28-113.152-79.744-88.192 1.92-27.136a293.12 293.12 0 0 0 0-40.256l-1.92-27.136 79.808-88.128-65.344-113.152-116.288 24.96-22.592-15.232a287.616 287.616 0 0 0-34.752-20.096l-24.448-11.904L577.344 128zM512 320a192 192 0 1 1 0 384 192 192 0 0 1 0-384zm0 64a128 128 0 1 0 0 256 128 128 0 0 0 0-256z" />
          </svg>
          </span>
            <span v-if="currentPage==='open'" class="helper-btn-create" @click.stop="setCurrentPage('create')">
              <svg width="20px" height="20px" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M5 12H19" stroke="#323232" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
                <path d="M12 5L12 19" stroke="#323232" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
              </svg>
            </span>
            <span v-if="currentPage==='open'" class="helper-btn-close" @click.stop="backToPrevPage()">
              <svg width="20px" height="20px" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path fill-rule="evenodd" clip-rule="evenodd"
                  d="M19.207 6.207a1 1 0 0 0-1.414-1.414L12 10.586 6.207 4.793a1 1 0 0 0-1.414 1.414L10.586 12l-5.793 5.793a1 1 0 1 0 1.414 1.414L12 13.414l5.793 5.793a1 1 0 0 0 1.414-1.414L13.414 12l5.793-5.793z"
                  fill="#000000" />
              </svg>
            </span>
            <span v-if="isNoOpen" class="helper-btn-back" @click.stop="backToPrevPage()">
              <svg width="20px" height="20px" viewBox="0 0 1024 1024" xmlns="http://www.w3.org/2000/svg">
                <path fill="#000000" d="M224 480h640a32 32 0 1 1 0 64H224a32 32 0 0 1 0-64z" />
                <path fill="#000000"
                  d="m237.248 512 265.408 265.344a32 32 0 0 1-45.312 45.312l-288-288a32 32 0 0 1 0-45.312l288-288a32 32 0 1 1 45.312 45.312L237.248 512z" />
              </svg>
            </span>
          </div>
        </div>
        <div v-if="currentPage==='open'" class="helper-main-list">
          <div v-for="group in groupList" class="group">
            <div class="group-title">
              <span>{{group.groupName}}</span>
              <span class="helper-btn-apply" @click="appleGroupValue(group.groupName)">
                <svg width="20px" height="20px" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                  <g id="Media / Play">
                    <path id="Vector"
                      d="M5 17.3336V6.66698C5 5.78742 5 5.34715 5.18509 5.08691C5.34664 4.85977 5.59564 4.71064 5.87207 4.67499C6.18868 4.63415 6.57701 4.84126 7.35254 5.25487L17.3525 10.5882L17.3562 10.5898C18.2132 11.0469 18.642 11.2756 18.7826 11.5803C18.9053 11.8462 18.9053 12.1531 18.7826 12.4189C18.6418 12.7241 18.212 12.9537 17.3525 13.4121L7.35254 18.7454C6.57645 19.1593 6.1888 19.3657 5.87207 19.3248C5.59564 19.2891 5.34664 19.1401 5.18509 18.9129C5 18.6527 5 18.2132 5 17.3336Z"
                      stroke="#000000" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
                  </g>
                </svg>
              </span>
            </div>
            <div v-for="action in group.actionList" class="action">
              <span class="action-name" @click="applyValue(group.groupName, action.name)">{{action.name}}</span>
              <span class="action-edit" @click="retrieveActionForEdit(group.groupName, action.name)">
                <svg width="20px" height="20px" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                  <path d="M13 21H21" stroke="#323232" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
                  <path
                    d="M20.0651 7.39423L7.09967 20.4114C6.72438 20.7882 6.21446 21 5.68265 21H4.00383C3.44943 21 3 20.5466 3 19.9922V18.2987C3 17.7696 3.20962 17.2621 3.58297 16.8873L16.5517 3.86681C19.5632 1.34721 22.5747 4.87462 20.0651 7.39423Z"
                    stroke="#323232" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
                  <path d="M15.3097 5.30981L18.7274 8.72755" stroke="#323232" stroke-width="2" stroke-linecap="round"
                    stroke-linejoin="round" />
                </svg>
              </span>
            </div>
          </div>
        </div>
        <div v-else-if="['create','edit'].includes(currentPage)" :class="helperCreateEditClassObj">
          <div class="input-group">
            <div class="input-item" :class="groupErrorClassObj">
              <label for="group">Group</label>
              <input id="group" v-model="action.group" :disabled="currentPage==='edit'" type="text" />
            </div>
            <div class="input-item" :class="nameErrorClassObj">
              <label for="name">Name</label>
              <input id="name" v-model="action.name" :disabled="currentPage==='edit'" type="text" />
            </div>
            <div class="input-item" :class="selectorErrorClassObj">
              <label for="selector">Selector</label>
              <input id="selector" v-model="action.selector" type="text" />
            </div>
            <div class="input-item" :class="valueErrorClassObj">
              <label for="value">Value</label>
              <span>
                <span>
                  <input id="value" :disabled="action.value.text.length>0" v-model="action.value.click" type="checkbox" />
                </span>
                <span>&nbsp;Click Event</span>
              </span>
              <input id="value" :disabled="action.value.click" v-model="action.value.text" type="text" />
            </div>
          </div>
          <div class="action-button-area">
            <div class="action-button">
              <span v-if="currentPage==='edit'" class="delete-button" @click="deleteAction">
                Delete
              </span>
              <span class="save-button" @click="saveAction"> Save </span>
              <span class="clear-button" @click="clearAction"> Clear </span>
            </div>
          </div>
        </div>
        <div v-else-if="currentPage==='setting'" class="helper-setting">
        <div class="upload-setting" @click="uploadClick">
          <svg width="40px" height="40px" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M12 8L12 16" stroke="#323232" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
            <path d="M15 11L12.087 8.08704V8.08704C12.039 8.03897 11.961 8.03897 11.913 8.08704V8.08704L9 11" stroke="#323232"
              stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
            <path d="M3 15L3 16L3 19C3 20.1046 3.89543 21 5 21L19 21C20.1046 21 21 20.1046 21 19L21 16L21 15" stroke="#323232"
              stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
          </svg>
          <input id="input-upload-file" type="file" accept="application/json" @change="uploadSetting">
        </div>
        <div class="download-setting" @click="downloadSetting">
          <svg width="40px" height="40px" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
            <g id="Interface / Download">
              <path id="Vector" d="M6 21H18M12 3V17M12 17L17 12M12 17L7 12" stroke="#000000" stroke-width="2"
                stroke-linecap="round" stroke-linejoin="round" />
            </g>
          </svg>
        </div>
      </div>
      </div>
    `,
  });
  app.mount(vContainer);
})();
